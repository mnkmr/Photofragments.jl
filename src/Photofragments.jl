module Photofragments

if VERSION < v"1.3"
    using Random: AbstractRNG, MersenneTwister
else
    using Random: AbstractRNG, default_rng
end

export
    Photofragment,
    random_particle,
    random_particle!,
    zare,
    rakitzis,
    gauss,
    muckerman

# Avogadro number (mol⁻¹)
const NA = 6.02214076e23


if VERSION < v"1.3"
    const THREAD_RNGs = MersenneTwister[]

    function __init__()
        for i in 1:Threads.nthreads()
            push!(THREAD_RNGs, MersenneTwister())
        end
    end

    default_rng(tid::Int) = THREAD_RNGs[tid]
    default_rng() = default_rng(Threads.threadid())
end


"""
    Photofragment(m, z, v, Ω, Θ, P)

Type for a photofragment particle, mainly for Monte Carlo simulations.
It has mass(m), charge(z), and velocity(v, Ω, Θ). The definition of Ω and Θ
follow those in [P. Rakitzis et al., Chem. Phys. Lett. 372 (2003) 187-194]
"""
mutable struct Photofragment
    m::Float64
    z::Int
    v::Float64
    Ω::Float64
    Θ::Float64
end


"""
    random_particle(m, z, vmin, vmax)

Generate a photofragment recoiling toward a random direction.
Give mass(m), charge(z), the minimum and maximam limit of velocity.
"""
random_particle(m, z, vmin, vmax) = random_particle(default_rng(), m, z, vmin, vmax)
function random_particle(rng::AbstractRNG, m, z, vmin, vmax)
    v, Ω, Θ = _random_particle(rng, vmin, vmax)
    Photofragment(m, z, v, Ω, Θ)
end


"""
    random_particle!(p, vmin, vmax)

Randomize a allocated photofragment.
Give the minimum and maximam limit of velocity.
"""
random_particle!(p, vmin, vmax) = random_particle!(default_rng(), p, vmin, vmax)
function random_particle!(rng::AbstractRNG, p, vmin, vmax)
    v, Ω, Θ = _random_particle(rng, vmin, vmax)
    p.v = v
    p.Ω = Ω
    p.Θ = Θ
    p
end


function _random_particle(rng::AbstractRNG, vmin, vmax)
    v = vmin + (vmax - vmin)*(1 - rand(rng))
    Ω = acosd(2*rand(rng) - 1)
    Θ = rand(rng)*360.0
    return v, Ω, Θ
end


"Calculate Legendre polynomials Pₗ(x)"
function legendre(l::Integer, x::T) where {T<:Real}
    if l == 0
        return one(float(T))
    elseif l == 1
        return float(x)
    elseif l == 2
        return (3*x^2 - 1)/2
    elseif l == 3
        return (5*x^3 - 3*x)/2
    elseif l == 4
        return (35*x^4 - 30*x^2 + 3)/8
    elseif l == 5
        return (63*x^5 - 70*x^3 + 15*x)/8
    elseif l == 6
        return (231*x^6 - 315*x^4 + 105*x^2 - 5)/16
    elseif l == 7
        return (429*x^7 - 693*x^5 + 315*x^3 - 35*x)/16
    elseif l == 8
        return (6435*x^8 - 12012*x^6 + 6930*x^4 - 1260*x^2 + 35)/128
    end

    Pₙ = legendre(7, x)
    Pₙ₊₁ = legendre(8, x)
    for n in 8:(l - 1)
        Pₙ₋₁ = Pₙ
        Pₙ = Pₙ₊₁
        Pₙ₊₁ = ((2n + 1)*x*Pₙ - n*Pₙ₋₁)/(n + 1)
    end
    Pₙ₊₁
end


# Angular distributions

"""
    zare(θ, n₁ => β₁, n₂ => β₂ ...)
    zare(p::Photofragment, args...) = zare(p.Ω, p.Θ, args...)

    zare(θ) = zare(θ, 2 => 2.0)
    zare(p::Photofragment) = zare(p, 2 => 2.0)

Calculate 3-dimensional photofragment distribution function of Zare's equation.
    I(θ) ∝ 1 + βₙPₙ(cosθ) + ...
where, n is an integer larger than 0. The order of Legendre polynomials are
assigned by using Pairs; for example `zare(θ, 2 => -1.0)` calculates
`1 + β₂P₂(cosθ)` with β₂ = -1.0.
"""
zare(θ::Real, args...) = _zare(cosd(θ), args)
zare(Ω::Real, Θ::Real, args...) = _zare(sind(Ω)*cosd(Θ), args)
zare(p::Photofragment, args...) = zare(p.Ω, p.Θ, args...)

zare(θ::Real) = zare(θ, 2 => 2.0)
zare(Ω::Real, Θ::Real) = zare(Ω, Θ, 2 => 2.0)
zare(p::Photofragment) = zare(p, 2 => 2.0)

function _zare(cosθ, args)
    I = 1.0
    for (l, β) in args
        I += β*legendre(l, cosθ)
    end
    I
end


"""
    rakitzis(Ω, Θ, Γ, Δ, Φ, α, χ, ϕ, c1, c2)
    rakitzis(Ω, Θ; Γ=90., Δ=0., Φ=0., α=180., χ=0., ϕ=0., c1=0., c2=0.)
    rakitzis(p::Photofragment; kwargs...) = rakitzis(p.Ω, p.Θ; kwargs...)

Calculate 3-dimensional photofragment distribution including molecular
orientation shown by Rakitzis.
Check [P. Rakitzis et al., Chem. Phys. Lett. 372 (2003) 187-194] for details.
Calcuate a photofragment distribution in laboratory frame,
based on Eq.(8) and Eq.(9a)-(9d) in the Ref.
"""
function rakitzis(Ω::Real, Θ::Real, Γ::Real, Δ::Real, Φ::Real,
                  α::Real, χ::Real, ϕ::Real, c1::Real, c2::Real)
    cosΩ = cosd(Ω)
    sinΩ = sind(Ω)
    sin²Ω = sinΩ*sinΩ

    cosΓ = cosd(Γ)
    sinΓ = sind(Γ)

    cosΘ = cosd(Θ)
    sinΘ = sind(Θ)

    cosΔ = cosd(Δ)
    sinΔ = sind(Δ)

    cosΦ = cosd(Φ)
    sinΦ = sind(Φ)

    cosα = cosd(α)
    cos²α = cosα*cosα
    sinα = sind(α)
    sin²α = sinα*sinα

    cosχ = cosd(χ)
    cos²χ = cosχ*cosχ
    sinχ = sind(χ)
    sin²χ = sinχ*sinχ

    cosϕ = cosd(ϕ)
    cos²ϕ = cosϕ*cosϕ
    sinϕ = sind(ϕ)
    sin²ϕ = sinϕ*sinϕ

    P₂χ = 0.5*(3*cos²χ - 1)
    P₂α = 0.5*(3*cos²α - 1)

    A = cosΩ*cosΓ + sinΩ*sinΓ*cosΘ
    A² = A*A

    B = cosΩ*cosΔ + sinΩ*sinΔ*cosd(Θ - Φ)
    B² = B*B

    C = (sin²Ω*cosΓ*cosΔ + sinΓ*sinΔ*cosΦ -
         sinΩ*cosΩ*(sinΔ*cosΓ*cosd(Φ - Θ) + sinΓ*cosΔ*cosΘ) -
         sin²Ω*sinΓ*sinΔ*cosd(Φ - Θ)*cosΘ)
    C² = C*C

    D = cosΩ*sinΓ*sinΔ*sinΦ - sinΩ*(sinΔ*cosΓ*sind(Φ - Θ) + sinΓ*cosΔ*sinΘ)
    D² = D*D

    ((1 + P₂χ*(3*A² - 1))*(1 + 2*c1*cosα*B + c2*P₂α*(3*B² - 1)) +
     6*A*sinχ*cosχ*sinα*(C*cosϕ - D*sinϕ)*(c1 + 3*c2*cosα*B) +
     9/8*c2*sin²χ*sin²α*((cos²ϕ - sin²ϕ)*(C² - D²) - 4*C*D*sinϕ*cosϕ))
end
rakitzis(Ω::Real, Θ::Real; Γ=90., Δ=0., Φ=0., α=180., χ=0., ϕ=0., c1=0., c2=0.) = rakitzis(Ω, Θ, Γ, Δ, Φ, α, χ, ϕ, c1, c2)
rakitzis(p::Photofragment; kwargs...) = rakitzis(p.Ω, p.Θ; kwargs...)


# Energy distributions

"""
    gauss(x, μ, σ)

Compute the Gaussian distribution with the mean `μ` and the standard deviation `σ`.
"""
gauss(x, μ, σ) = 1/(√(2π)*σ)*exp(-(x - μ)^2/(2*σ^2))


"""
    muckerman(Et::Real, Eavl::Real, a::Real, b::Real)
    muckerman(v::Real, vmax::Real, m::Real, M::Real, a::Real, b::Real)

Compute a velocity distribution which follows the Muckerman's equation of
energy distribution. [J. T. Muckerman, J. Phys. Chem. 93, (1989), 179–184.]
    I(ft) = ft^a*(1 - ft)^b
    ft = Et/Eavl
`Et` is the kinetic energy release, `Eavl` is the maximum available energy.
`m` and `M` are the mass of the fragment and the parent molecule in atomic unit.
"""
function muckerman(Et::Real, Eavl::Real, a::Real, b::Real)
    ft = Et/Eavl
    ft^a*(1 - ft)^b
end
function muckerman(v::Real, vmax::Real, m::Real, M::Real, a::Real, b::Real)
    Et = speed_to_energy(v, m, M)
    Eavl = speed_to_energy(vmax, m, M)
    muckerman(Et, Eavl, a, b)
end


# Translaters

"""
    v = pixel_to_speed(r, ppm, tof, N)

Translate pixel distance to fragment speed.

See also: [`speeddist`](@ref), [`pixel_to_energy`](@ref)

# Arguments
- `r`  : the vector of radii in pixel.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`  : a magnification factor of electrostatic optics.[1]

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v = pixel_to_speed(1:100, ppm, tof, 1.);
```

[1] Eppink, A. T. J. B.; Parker, D. H. Velocity Map Imaging of Ions and
    Electrons Using Electrostatic Lenses: Application in Photoelectron and
    Photofragment Ion Imaging of Molecular Oxygen. Rev. Sci. Instrum. 1997, 68
    (9), 3477.
"""
pixel_to_speed(r, ppm, tof, N) = r / N / ppm / tof


"""
    Et = speed_to_energy(v, m, M)

Translate fragment speed to center-of-mass kinetic energy release in kJ/mol.

See also: [`energy_to_speed`](@ref), [`speeddist`](@ref), [`energydist`](@ref)

# Arguments
- `v`: the fragment recoil speed in m/s.
- `m`: the mass of the fragment in atomic unit.
- `M`: the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> ppm = 20e3; # 20 pixel/mm = 20e3 pixel/m

julia> tof = 10.0e-6; # 10.0 μs = 10.0e-6 s

julia> v = pixel_to_speed(1:100, ppm, tof);

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> Et = speed_to_energy(v, m, M);  # in kJ/mol
```
"""
speed_to_energy(v, m, M) = 0.5*(m*1e-3/NA)*v*v*(M/(M - m))*1e-3


"""
    pixel_to_energy(r, m, M, ppm, tof, N)

Translate pixel distance to center-of-mass kinetic energy release in kJ/mol.

See also: [`pixel_to_speed`](@ref)

# Arguments
- `r`  : the vector of radii in pixel.
- `m`  : the mass of the fragment in atomic unit.
- `M`  : the mass of the parent molecule in atomic unit.
- `ppm`: pixel-per-meter (pixel/m).
- `tof`: time-of-flight in second.
- `N`  : a magnification factor of electrostatic optics.
"""
pixel_to_energy(r, m, M, ppm, tof, N) =
    speed_to_energy(pixel_to_speed(r, ppm, tof, N), m, M)


"""
    v = energy_to_speed(Et, m, M)

Translate center-of-mass kinetic energy release to fragment speed.

See also: [`speed_to_energy`](@ref), [`speeddist`](@ref), [`energydist`](@ref)

# Arguments
- `Et`: the kinetic energy release in kJ/mol.
- `m` : the mass of the fragment in atomic unit.
- `M` : the mass of the parent molecule in atomic unit.

# Example
```julia-repl
julia> using IonimageAnalysisCore

julia> Et, IEt = energydist(imagedata); # imagedata is provided by another package

julia> m, M = 127, 142; # For example, observing I⁺ of CH₃I -> CH₃ + I

julia> v = energy_to_speed(Et, m, M);
```
"""
function energy_to_speed(Et, m, M)
    M = M*1e-3/NA
    m = m*1e-3/NA
    sqrt(2(M - m)/(M*m)*Et*1e3)
end


end # module
