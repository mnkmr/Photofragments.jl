using Photofragments
using Statistics
using Test

@testset "Photofragments.jl" begin
    let p = Array{Photofragment,1}(undef, 10000)
        vmin = 0.0
        vmax = 100.0
        for i in eachindex(p)
            p[i] = random_particle(1, 1, vmin, vmax)
        end

        # p.v is uniformly distributed in vmin <= v < vmax
        v = map(p -> p.v, p)
        @test all(vmin .<= v .<= vmax)
        @test 49 < mean(v) < 51
        @test isapprox(std(v), 100/sqrt(12); rtol=0.01)

        # vector (r = 1, p.Ω, p.Θ) is distributed uniformly on a 3D sphere surface
        Ω = map(p -> p.Ω, p)
        Θ = map(p -> p.Θ, p)
        @test all(0 .<= Ω .<= 180)
        @test all(0 .<= Θ .<= 360)
        x = @. sind(Ω)*cosd(Θ)
        y = @. sind(Ω)*sind(Θ)
        z = @. cosd(Ω)
        @test -0.01 < mean(x) < 0.01
        @test -0.01 < mean(y) < 0.01
        @test -0.01 < mean(z) < 0.01
        @test isapprox(std(x), 2/sqrt(12); rtol=0.01)
        @test isapprox(std(y), 2/sqrt(12); rtol=0.01)
        @test isapprox(std(z), 2/sqrt(12); rtol=0.01)
    end

    let p = Array{Photofragment,1}(undef, 10000)
        vmin = 0.0
        vmax = 100.0
        for i in eachindex(p)
            p[i] = random_particle(1, 1, 1.0, 1.0)
        end
        for i in eachindex(p)
            random_particle!(p[i], vmin, vmax)
        end

        # p.v is uniformly distributed in vmin <= v < vmax
        v = map(p -> p.v, p)
        @test all(vmin .<= v .<= vmax)
        @test 49 < mean(v) < 51
        @test isapprox(std(v), 100/sqrt(12); rtol=0.01)

        # vector (r = 1, p.Ω, p.Θ) is distributed uniformly on a 3D sphere surface
        Ω = map(p -> p.Ω, p)
        Θ = map(p -> p.Θ, p)
        @test all(0 .<= Ω .<= 180)
        @test all(0 .<= Θ .<= 360)
        x = @. sind(Ω)*cosd(Θ)
        y = @. sind(Ω)*sind(Θ)
        z = @. cosd(Ω)
        @test -0.01 < mean(x) < 0.01
        @test -0.01 < mean(y) < 0.01
        @test -0.01 < mean(z) < 0.01
        @test isapprox(std(x), 2/sqrt(12); rtol=0.01)
        @test isapprox(std(y), 2/sqrt(12); rtol=0.01)
        @test isapprox(std(z), 2/sqrt(12); rtol=0.01)
    end
end
